import React from 'react';

import Mapa from '../Mapa';
import Menu from '../Menu';

import { 
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList 
} 
from '@react-navigation/drawer';
import { useFocusEffect } from '@react-navigation/native';

import {
  AsyncStorage,
  Image,
  Text,
  TouchableOpacity,
  View, 
} from 'react-native';

const Drawer = createDrawerNavigator();

export default function Main(props) {
  function CustomDrawerContent(props) {
    return (
      <View style={{ flex: 1, flexDirection: 'column', alignItems: 'stretch'}}>
        <View style={{backgroundColor: '#e40063', flex: 1}}>
          <TouchableOpacity onPress={() => {
            console.log('Press')
            }}>
            {/* <Image source={{uri: foto.uri }} 
            style={{ width: 100, height: 100, marginLeft: 5 }} /> */}
            <Image
              source={require('../../assets/img/logo.png')}
              style={{ width: 100, height: 100, marginLeft: 5 }}
            />
          </TouchableOpacity>
        </View>
        <View style={{backgroundColor: 'yellow', flex: 2}} >
          <TouchableOpacity style={{ backgroundColor: 'white' }} onPress={() => {
            console.log('press');
            AsyncStorage.removeItem('user', (err, result) =>{
              props.navigation.navigate('Login');
            });
            }}>
            <Text>Limpar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  useFocusEffect(() => {
    AsyncStorage.getItem('user', (err, result) => {
      console.log(result);
      return result;
    });
  });

  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Mapa" component={Mapa} />
      <Drawer.Screen name="Menu" component={Menu} />
    </Drawer.Navigator>
  );
}
