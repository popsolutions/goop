import React,  { useState, useRef } from 'react';
import InputScrollView from 'react-native-input-scroll-view';
import { SafeAreaView } from 'react-native-safe-area-context';
import Constants from 'expo-constants';
import { useFocusEffect } from '@react-navigation/native';
import Odoo from 'react-native-odoo-promise-based';

import * as yup from 'yup';
import {Formik} from 'formik';

import { 
  AsyncStorage, 
  Dimensions, 
  Image, 
  Keyboard,
  TextInput, 
  Text, 
  TouchableHighlight, 
  View 
} from 'react-native';

import styles from './styles.js';

export default function Login(props) {
  let {height, width} = Dimensions.get('window');
  const [message, setMessage] = useState('');
  const [user, setUser] = useState({});

  const inputs = {};
  const baseUrl = "goop.popsolutions.com.br";
  let initialValues = {email: '', password: ''};

  function focusNextField(id) {
    if (inputs[id].getElement) {
      inputs[id].getElement().focus();
    } else {
      inputs[id].focus();
    }
  }

  useFocusEffect(() => {
    AsyncStorage.removeItem('foto');
    AsyncStorage.getItem('user', (err, result) => {
      if(result !== null){
        setUser(result);
        // todo: checar se usuario tem permissao com backend
        //if(){
        props.navigation.navigate('Main');
        //}
      }
      return result;
    });
    initialValues = {email: '', password: ''};
  });

  async function getOdoo(login, password){
    const odoo = new Odoo({
      host: baseUrl,
      database: 'goop-dev-br',
      username: login,
      password: password, 
    });

    return odoo;
  }

  async function connectAPI(odoo){
    const odoo_res = odoo.connect()
      .then(res => { 
        console.log(res);
        if(res.hasOwnProperty('data')){
          if(res.data.uid == false){
            throw 'Email e/ou senha incorretos';
          }else{
            return {code: 1, uid: res.data.uid};
          }
        }
      })
      .catch(e => {
        console.log(e);
        return {code: 0, e: e};
      });

    return odoo_res;
  }

  async function sendLogin(data){
    const odoo = await getOdoo(data.email, data.password);
    const res = await connectAPI(odoo);
    console.log(res);
    if(res.code == 1){
      const uid = res.uid;
      console.log(uid);

      console.log(uid);

      const  params = {
        ids: [uid],
        fields: [
          'name', 
          'image',
          'image_medium',
          'function', 
          'education_level', 
          'gender', 
          'missions_count' 
        ]
      }

      console.log(params);

      odoo.get('res.users', params)
        .then(response => { 
          console.log('response ' + JSON.stringify(response));
          AsyncStorage.setItem('user', response.data);
        })
        .catch(e => { 
          console.log('e ' + JSON.strinfigy(e));
        })

      props.navigation.navigate('Main');
    }else{
      setMessage(res.e);
    }
  }

  return (
    <SafeAreaView style={{display: 'flex', height: height, backgroundColor: '#fff'}}>
      <InputScrollView 
        keyboardShouldPersistTaps="always" >
        <Formik
          enableReinitialize={true}
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={initialValues}
          onSubmit={values => {
            sendLogin(values);
          }}
          validationSchema={yup.object().shape({
            email: yup
              .string()
              .email('Digite um e-mail válido')
              .required('Preencha o Email'),
            password: yup
              .string()
              .min(6, 'A senha deve ter no mínimo 6 caracteres')
              .required('Preencha a Senha'),
          })}>
          {({
            values,
            handleChange,
            errors,
            setFieldTouched,
            touched,
            handleSubmit,
            handleReset
          }) => (
            <View style={[{flex: 1, height: height- Constants.statusBarHeight}]}>
              <View style={[styles.imageArea]}>
                <Image
                  source={require('../../assets/img/logo.png')}
                  style={{width: width/2.5, height: width/2.5}}
                />
              </View>
              <View style={styles.inputArea}>
                <View style={{height: 100}}>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    autoCapitalize='none'
                    value={values.email}
                    onChangeText={handleChange('email')}
                    onChange={() => setMessage('') }
                    onFocus={() => setFieldTouched('email',false)}
                    style={styles.input}
                    keyboardType="email-address"
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('2');
                    }}
                    returnKeyType={'next'}
                    ref={input => {
                      inputs['1'] = input;
                    }}
                  />
                  {touched.email && errors.email && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.email}
                    </Text>
                  )}
                </View>
                <View style={{height: 100}}>
                  <Text style={styles.label}>Senha</Text>
                  <TextInput
                    value={values.password}
                    onChangeText={handleChange('password')}
                    onChange={() => {
                      setFieldTouched('password',false);
                      setMessage('')
                    }}
                    onFocus={() => setFieldTouched('password',false)}
                    secureTextEntry={true}
                    style={styles.input}
                    ref={input => {
                      inputs['2'] = input;
                    }}
                  />
                  {touched.password && errors.password && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.password}
                    </Text>
                  )}
                  <Text style={[styles.label, styles.error]}>{message}</Text> 
                </View>
              </View>
              <View style={{ flex:1, flexGrow: 3, justifyContent: 'space-around'}}>
                <TouchableHighlight
                  style={styles.pinkButton}
                  onPress={() => {
                    Keyboard.dismiss();
                    handleSubmit();
                  }}
                  underlayColor="#fff">
                  <Text style={styles.submitText}>Entrar</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  style={styles.lightButton}
                  onPress={() => {
                    handleReset();
                    setMessage('');
                    props.navigation.navigate('CadastroForm1')
                  }}
                  underlayColor="#fff">
                  <Text style={styles.darkText}>Cadastre-se agora</Text>
                </TouchableHighlight>
              </View>
            </View>
          )}
        </Formik>
      </InputScrollView>
    </SafeAreaView>
  );
}