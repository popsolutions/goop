import React, { useState, useEffect } from 'react';
import { AsyncStorage, Text, View, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';

export default function Foto(props) {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.front);

  const [text, setText] = useState('Tirar Selfie');
  const [foto, setFoto] = useState('');

  let camera = '';

  async function takePicture(){
    try{
      let photo = await camera.takePictureAsync({quality: 0.3, base64: true});
      camera.pausePreview();
      setFoto({uri: photo.uri, base64: photo.base64});
      setText('Enviar');
      return true;
    }catch(e){
      return false;
    }
  }

  function cancelar(){
    camera.resumePreview();
    setText('Tirar Selfie');
  }

  function salvar(){
    AsyncStorage.setItem('foto', JSON.stringify({foto: foto}), () => {
      props.navigation.navigate('CadastroForm1');
    });
  }

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>Sem acesso à Câmera</Text>;
  }
  return (
    <View style={{ flex: 1 }}>
      <Camera 
        style={{ flex: 1 }} 
        type={type} 
        ref={ref => {camera = ref}} 
        ratio='4:3'
        >
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row'
          }}>

          {text == 'Tirar Selfie' &&
          <TouchableOpacity
            style={{
              flex: 1,
              alignSelf: 'flex-end',
              flexDirection: 'row',
              alignItems: 'center',
              flexGrow: 1
            }}
            onPress={() => {
              const pic = takePicture();
            }}>
            <Text style={{ 
              backgroundColor: '#79B012',
              fontSize: 18, 
              marginBottom: 0,
              padding: 15, 
              flexGrow: 1,
              alignSelf: 'stretch',
              color: 'white',
              textAlign: 'center'
            }}>{text}</Text>
          </TouchableOpacity>
          }

          {text == 'Enviar' && 
            <>
              <TouchableOpacity
              style={{
                flex: 1,
                alignSelf: 'flex-end',
                flexDirection: 'row',
                alignItems: 'center',
                flexGrow: 1
              }}
              onPress={() => {
                salvar()
              }}>
                <Text style={{ 
                  backgroundColor: '#79B012',
                  fontSize: 18, 
                  marginBottom: 0,
                  padding: 15, 
                  alignSelf: 'stretch',
                  color: 'white',
                  flexGrow: 1,
                  textAlign: 'center'
                }}>Salvar</Text>
              </TouchableOpacity>
              <TouchableOpacity
              style={{
                flex: 1,
                alignSelf: 'flex-end',
                flexDirection: 'row',
                alignItems: 'center',
                flexGrow: 1
              }}
              onPress={() => {
                cancelar()
              }}>
                <Text style={{ 
                  backgroundColor: '#B02C12',
                  fontSize: 18, 
                  marginBottom: 0,
                  padding: 15, 
                  alignSelf: 'stretch',
                  color: '#fff',
                  flexGrow: 1,
                  textAlign: 'center'
                }}>Cancelar</Text>
              </TouchableOpacity>
              
            </>
          }
        </View>
      </Camera>
    </View>
  );
}