import React from 'react';
import InputScrollView from 'react-native-input-scroll-view';
import { SafeAreaView } from 'react-native-safe-area-context';

import {
  Dimensions
} from 'react-native';

export default function Termos(props) {
  let {height, width} = Dimensions.get('window');

  return (
    <SafeAreaView 
      style={{display: 'flex', height: height, backgroundColor: '#fff'}}>
      <InputScrollView
        keyboardShouldPersistTaps="always"></InputScrollView>
    </SafeAreaView>
  );
}