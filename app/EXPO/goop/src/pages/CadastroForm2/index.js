import React, { useState } from 'react';
import InputScrollView from 'react-native-input-scroll-view';

import * as yup from 'yup';
import {Formik} from 'formik';
import {TextInputMask} from 'react-native-masked-text';
import { useFocusEffect } from '@react-navigation/native';
import Odoo from 'react-native-odoo-promise-based';
import {CheckBox} from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';

import {
  AsyncStorage,
  Dimensions,
  Keyboard,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';

import styles from './styles.js';

export default function CadastroForm2(props) {
  const inputs = {};
  const baseUrl = "goop.popsolutions.com.br";

  const [aceito, setAceito] = useState(false);
  const [user, setUser] = useState({});
  const [address, setAddress] = useState({});

  let {height, width} = Dimensions.get('window');

  const initialValues2 = {
    zip: '',
    education_level: '',
    function: '',
    accept: aceito,
    password: '',
    passwordConfirm: ''
  };

  const odoo = new Odoo({
    host: baseUrl,
    database: 'goop-dev-br',
    username: 'goop@popsolutions.co',
    password: 'DHV?>DZp&7F7",kx', 
  });

  odoo.connect();

  async function storeData(name, object){
    AsyncStorage.mergeItem(name, JSON.stringify(object));    
    let b = await getItemStored(name);
    return b;
  }

  async function getItemStored(name){
    let r = await AsyncStorage.getItem(name, function(res){
      return res;
    });
    return r;
  }

  async function createUserObj(obj){
    let us = JSON.parse(obj);
    let foto = await getItemStored('foto');

    let picture = JSON.parse(foto);

    us.image = picture.foto.base64;
    if(Object.keys(address).length > 0){
      us.city = address.localidade;
      us.street = address.logradouro;
      us.district = address.bairro;
      us.state = address.state;
    }
    us.birthdate = new Date(us.birthdate);

    return us;
  }

  function createUser(user){
    console.log('createUser');

    storeData('user', user, {}).then(async(res) => {
      let user_obj = await createUserObj(res);

      odoo.connect().then(function(res){
        console.log(res);
        odoo.create('res.partner', user_obj)
        .then(res => {
          console.log(res);
          if(res.success){
            storeData('user', user_obj);
            props.navigation.navigate('Main');
          }else{

          }
        })
        .catch(e => {
          //Alert.alert(e.)
          console.log(e);
        })

      })
      
    });
  }

  function focusNextField(id) {
    console.log(typeof(inputs[id].getElement) == 'undefined');
    if(inputs[id].getElement) {
      inputs[id].getElement().focus();
    } else {
      inputs[id].focus();
    }
  }

  useFocusEffect(() => {
    console.log('useFocusEffect CadastroForm 2');
    AsyncStorage.getItem('user', (err, result) => {
      if(result !== null){
        setUser(result);
      }
    });
  });

  return (
    <SafeAreaView style={[styles.white, { flexGrow: 1}]}>
      <InputScrollView 
          keyboardAvoidingViewProps={{keyboardVerticalOffset: 100}}
          keyboardShouldPersistTaps="handled"
          useAnimatedScrollView={true}>
          <Formik
            validateOnBlur={false}
            validateOnChange={false}
            initialValues={initialValues2}
            onSubmit={values => createUser(values) }
            validationSchema={yup.object().shape({
              zip: yup
                .string()
                .min(8, 'O CEP deve ter 8 números')
                .required('Preencha o CEP')
                .test({
                  test: function(value){
                    return fetch('https://viacep.com.br/ws/'+ value+ '/json/')
                      .then(res => {
                        return res.json().then(function(r){
                          if(r.erro){
                            return false;
                          }else{
                            setAddress(r);
                            return true;
                          }
                        })
                      })
                  },
                  message:'CEP Inválido'
                }),
              education_level: yup.string().required('Preencha a Escolaridade'),
              function: yup.string().required('Preencha a Profissão'),
              password: yup
                .string()
                .required('Preencha a Senha')
                .min(8, 'A senha deve conter 8 caracteres')
                .max(8, 'A senha deve conter 8 caracteres'),
              passwordConfirm: yup
                .string()
                .required('Preencha a Confirmação da Senha')
                .oneOf([yup.ref('password'), null], 'As senhas devem ser iguais'),
              accept: yup
                .boolean()
                .required('Você deve aceitar os Termos de Uso')
                .oneOf([true], 'Você deve aceitar os Termos de Uso'),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
            }) => (
              <View style={[styles.white, 
                {
                  width: width
                }]}>
                <View>
                  <View style={[styles.titleView, styles.white]}>
                    <Text style={styles.title}>Cadastro</Text>
                  </View>
                  <View style={{height: 95}}>
                    <Text style={styles.label}>CEP Residencial</Text>
                    <TextInputMask
                      type={'custom'}
                      options={{
                        mask: '99999-999',
                      }}
                      keyboardType='numeric'
                      style={styles.input}
                      value={values.zip}
                      blurOnSubmit={false}
                      onSubmitEditing={() => {
                        focusNextField('8');
                      }}
                      returnKeyType={'next'}
                      onChangeText={handleChange('zip')}
                      onFocus={()=> {
                        setFieldTouched('zip',false);
                      }}
                    />
                    {touched.zip && errors.zip && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.zip}
                      </Text>
                    )}
                  </View>
                  <View style={{height: 95}}>
                    <Text style={styles.label}>Escolaridade</Text>
                    <TextInput
                      style={styles.input}
                      maxLength={40}
                      value={values.education_level}
                      blurOnSubmit={false}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['8'] = input;
                      }}
                      onSubmitEditing={() => {
                        focusNextField('9');
                      }}
                      keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                      onChangeText={handleChange('education_level')}
                      onFocus={()=>setFieldTouched('education_level',false)}
                    />
                    {touched.education_level && errors.education_level && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.education_level}
                      </Text>
                    )}
                  </View>
                  <View style={{height: 95}}>
                    <Text style={styles.label}>Profissão</Text>
                    <TextInput
                      maxLength={40}
                      style={styles.input}
                      value={values.function}
                      blurOnSubmit={false}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['9'] = input;
                      }}
                      onSubmitEditing={() => {
                        focusNextField('10');
                      }}
                      keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                      onChangeText={handleChange('function')}
                      onFocus={()=>setFieldTouched('function',false)}
                    />
                    {touched.function && errors.function && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.function}
                      </Text>
                    )}
                  </View>
                  <View style={{height: 95}}>
                    <Text style={styles.label}>Senha</Text>
                    <TextInput 
                      maxLength={8}
                      style={styles.input}
                      value={values.password}
                      blurOnSubmit={false}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['10'] = input;
                      }}
                      onSubmitEditing={() => {
                        focusNextField('11');
                      }}
                      onChangeText={handleChange('password')}
                      onFocus={()=>setFieldTouched('password',false)}
                      secureTextEntry={true}
                    />
                    {touched.password && errors.password && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.password}
                      </Text>
                    )}
                  </View>
                  <View style={{height: 95}}>
                    <Text style={styles.label}>Confirmar Senha</Text>
                    <TextInput 
                      maxLength={8}
                      style={styles.input}
                      value={values.passwordConfirm}
                      blurOnSubmit={false}
                      returnKeyType={'next'}
                      onChangeText={handleChange('passwordConfirm')}
                      onFocus={()=>setFieldTouched('passwordConfirm',false)}
                      secureTextEntry={true}
                      ref={input => {
                        inputs['11'] = input;
                      }}
                      onSubmitEditing={() => {
                        Keyboard.dismiss()
                      }}
                    />
                    {touched.passwordConfirm && errors.passwordConfirm && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.passwordConfirm}
                      </Text>
                    )}
                  </View>
                  <View style={{height: 95}}>
                    <CheckBox
                      title={<View style={{flex: 1, flexDirection: 'row'}}>
                        <Text>Aceito os </Text>
                        <TouchableHighlight
                          underlayColor='white'
                          activeOpacity={0.3}
                          onPress={()=> props.navigation.navigate('Termos') }>
                          <Text style={{textDecorationLine: 'underline'}}>Termos de Uso</Text>
                        </TouchableHighlight>
                      </View>}
                      containerStyle={[
                        styles.label,
                        styles.childrenContainerStyle,
                        {height: 22, marginTop: 20}
                      ]}
                      checkedColor='#86d151'
                      uncheckedColor='#858585'
                      textStyle={{fontWeight: 'normal', color: '#202020'}}
                      size={28}
                      checked={values.accept}
                      onIconPress={() => {
                        values.accept = !values.accept;
                        setAceito(!values.accept);
                        setFieldTouched('accept', false);
                      }}
                    />
                    {touched.accept && errors.accept 
                      && values.accept == false && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.accept} 
                      </Text>
                    )}
                  </View>
                  <View style={[styles.alignItemsCenter, {height: 65}]}></View>
                </View>
                <View>
                  <TouchableHighlight
                    style={styles.button}
                    onPress={()=>{
                      Keyboard.dismiss();
                      handleSubmit();
                    }}
                    underlayColor="#fff">
                    <Text style={styles.submitText}>Finalizar</Text>
                  </TouchableHighlight>
                </View>
                {/* {Object.values(errors).map(msg => {
                  <>
                    <Text>{msg}</Text>
                  </>
                })} */}
              </View>
            )}
        </Formik>
      </InputScrollView>
    </SafeAreaView>
  );
}