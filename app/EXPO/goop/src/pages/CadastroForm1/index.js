import React, {useState} from 'react';
import InputScrollView from 'react-native-input-scroll-view';
import * as yup from 'yup';
import {Formik} from 'formik';
import {TextInputMask} from 'react-native-masked-text';
import ModalSelector from 'react-native-modal-selector';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useFocusEffect } from '@react-navigation/native';
import Moment from 'moment';

import {
  AsyncStorage,
  Dimensions,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';

import styles from './styles.js';

export default function CadastroForm1(props) {
  const [foto, setFoto] = useState('');

  const [modalVisible, setModalVisible] = useState(false);
  const [cpfvalido, setCPFvalido] = useState(false);
  const [genero, setGenero] = useState('');
  const [editablegender, setEditableGender] = useState(true);

  const inputs = {};

  const data = [
    {key: 0, label: 'Feminino'},
    {key: 1, label: 'Masculino'},
  ];

  let {height, width} = Dimensions.get('window');

  const initialValues = {
    image: '',
    name: '',
    cpf_cnpj: '',
    birthdate: '',
    gender: '',
    email: '',
    mobile: ''
  };

  function focusNextField(id) {
    try{
      if(inputs[id].getElement) {
        inputs[id].getElement().focus();
      } else {
        inputs[id].focus();
      }
    }catch(e){
      console.log(e);
    }
  }

  function storeData(name, object){
    AsyncStorage.mergeItem(name, JSON.stringify(object), () => {
      AsyncStorage.getItem(name, (err, result) => {
        console.log(result);
        return;
      });
    });
  }

  function submit(values) {
    storeData('user', values, {});
    props.navigation.navigate('CadastroForm2')
  }

  function getFoto(){
    return AsyncStorage.getItem('foto', (err, result) => {
      if(result !== null){  
        const img = JSON.parse(result);
        if(img !== null){
          setFoto(img.foto);
        }
      }
    });
  }

  useFocusEffect(() => {
    getFoto();
  }, [foto]);

  return (
    <SafeAreaView style={[styles.white, { flexGrow: 1}]}>
      <InputScrollView 
        keyboardAvoidingViewProps={{keyboardVerticalOffset: 100}}
        keyboardShouldPersistTaps="handled"
        useAnimatedScrollView={true}
        >
        <Formik
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={initialValues}
          onSubmit={values => submit(values) }
          validationSchema={yup.object().shape({
            image: yup.string().required('Envie a foto'),
            name: yup
              .string()
              .required('Preencha o Nome')
              .matches(/^[a-zA-Z]+ (([a-zA-Z ])+[a-zA-Z]*)+$/g, 'Nome Incompleto'),
            cpf_cnpj: yup
              .string()
              .min(10, 'O CPF deve ter no mínimo 10 caracteres')
              .required('Preencha o CPF'),
            birthdate: yup
              .string()
              .min(10, 'DD/MM/AAAA')
              .required('Preencha a Data')
              .test({
                test: value => {
                  const anos = Moment().diff(Moment(value, 'DD/MM/YYYY'),'years');
                  return anos >= 18 && anos <= 120;
                },
                message:'Data Inválida'
              }),
            email: yup
              .string()
              .email('Email Inválido')
              .required('Preencha o Email'),
            mobile: yup
              .string()
              .required('Preencha o Celular')
              .matches(/\((1[1-9]|2[12478]|3[1-8]|4[1-9]|5[13-5]|6[1-9]|7[13-5]|7[79]|8[1-9]|9[1-9]{1})\) [9](\d{4}-\d{4})/g,"Número Inválido")
              .test({
                test: value => {
                  if (typeof(value) !== "undefined"){
                    const cel = value.substr(6,15);
                    return (cel.match(/(\d)\1{3}-(\d)\1{3}/g)) ? false : true;
                  }
                  return false;
                },
                message:'Número Inválido'
              }),
              gender: yup.string().required('Selecione o Gênero'),
          })}>
          {({
            values,
            handleChange,
            errors,
            setFieldTouched,
            touched,
            handleSubmit,
            setFieldValue
          }) => (
            <View style={[styles.white, 
              {
                justifyContent: 'space-between',
                flex: 1,
                flexGrow: 1,
              }]}>
              <View>
                <View style={[styles.titleView, styles.white]}>
                  <Text style={styles.title}>Cadastro</Text>
                </View>
                <View style={[styles.alignItemsCenter, {height: 150, marginBottom: 10}]}>
                  <TouchableOpacity
                    onPress={() => {
                      setFieldTouched('foto', false);
                      props.navigation.navigate('Foto');
                    }}>
                    <View>
                      {
                        foto !== '' ? (
                        <>
                          <Image source={{uri: foto.uri }} style={styles.image} />
                          <TextInput 
                            value={values.image} 
                            style={{height: 0, opacity: 0, width: 0}}
                            onChangeText={handleChange('image')}
                            onLayout={(layout)=>{
                              setFieldValue('image', foto.uri);
                              setFieldTouched('image', false);
                            }}
                            />
                        </>
                        ) : 
                        (<Image 
                          source={require('../../assets/img/placeholder.png')} 
                          style={styles.image} />
                        )
                      }
                    </View>
                    <Text style={styles.imageLabel}>Foto</Text>
                  </TouchableOpacity>
                  {touched.image && errors.image && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.image}
                    </Text>
                  )}
                </View>
                <View style={{height: 95, flexShrink: 0}}> 
                  <Text style={styles.label}>Nome Completo</Text>
                  <TextInput
                    autoCapitalize='sentences'
                    maxLength={60}
                    blurOnSubmit={false}
                    keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                    onChangeText={handleChange('name')}
                    returnKeyType={'next'}
                    style={styles.input}
                    value={values.nome}
                    onSubmitEditing={() => {
                      focusNextField('2');
                    }}
                    onFocus={() => setFieldTouched('name',false)}
                    ref={input => {
                      inputs['1'] = input;
                    }}
                  />
                  {touched.name && errors.name && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.name}
                    </Text>
                  )}
                </View>
                <View style={{height: 95 }}>
                  <Text style={styles.label}>CPF</Text>
                  <TextInputMask
                    type={'cpf'}
                    style={styles.input}
                    value={values.cpf_cnpj}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('3');
                    }}
                    returnKeyType={'next'}
                    onChangeText={handleChange('cpf_cnpj')}
                    onBlur={() => {
                      if(inputs['2'].isValid() == false){
                        setCPFvalido(false);
                      }else{
                        setCPFvalido(true);
                      }
                    }}
                    onFocus={() => setFieldTouched('cpf_cnpj',false)}
                    ref={ref => (inputs['2'] = ref)}
                  />
                  {touched.cpf_cnpj && errors.cpf_cnpj && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.cpf_cnpj}
                    </Text>
                  )}
                  {touched.cpf_cnpj && !errors.cpf_cnpj && cpfvalido == false && (
                    <Text style={[styles.label, styles.error]}>
                      CPF Inválido
                    </Text>
                  )}
                </View>
                <View style={[styles.twoRows, {height: 95}]}>
                  <View style={[styles.column, {height: 95}]}>
                    <Text style={styles.label}>Nascimento</Text>
                    <TextInputMask
                      type={'datetime'}
                      style={[styles.input,{ paddingLeft: 0}]}
                      value={values.birthdate}
                      blurOnSubmit={false}
                      onSubmitEditing={() => {
                        Keyboard.dismiss();
                        setTimeout(()=>{
                          setModalVisible(true);
                        }, 200);
                      }}
                      options={{
                        format: 'DD/MM/YYYY',
                      }}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['3'] = input;
                      }}
                      onChangeText={handleChange('birthdate')}
                      onFocus={() => {
                        setFieldTouched('birthdate',false);
                      }}
                    />
                    {touched.birthdate && errors.birthdate && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.birthdate}
                      </Text>
                    )}
                  </View>
                  <View style={[styles.column, {height: 95}]}>
                    <Text style={styles.label}>Gênero</Text>
                    <TextInput 
                      value={values.gender} 
                      style={{opacity: 0, height: 0}}
                      editable={editablegender}
                      ref={ref => { inputs['4'] = ref }} 
                      onFocus={() => { 
                        setEditableGender(false);
                        focusNextField('5');
                      }}/>
                    <ModalSelector
                      visible={modalVisible}
                      animationType='none'
                      data={data}
                      initValue={genero}
                      style={styles.modalSelector}
                      optionContainerStyle={styles.white}
                      cancelContainerStyle={styles.cancelContainerStyle}
                      touchableActiveOpacity={1}
                      optionStyle={styles.optionStyle}
                      optionTextStyle={styles.optionTextStyle}
                      selectTextStyle={styles.selectTextStyle}
                      touchableStyle={styles.touchableStyle}
                      selectStyle={styles.selectStyle}
                      childrenContainerStyle={styles.childrenContainerStyle}
                      initValueTextStyle={styles.initValueTextStyle}
                      onModalOpen={() => { 
                        Keyboard.dismiss();
                        setFieldTouched('gender', false);
                        setModalVisible(true);
                      }}
                      onChange={(item)=>{
                        if (item.key === 0 || item.key === 1) {
                          setFieldValue('gender',item.label);
                          setGenero(item.label);
                          handleChange('gender');
                          setEditableGender(true);
                        }
                      }}
                      onModalClose={item => {
                        focusNextField('4');
                        setModalVisible(false);
                      }}
                    />
                    {touched.gender && errors.gender && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.gender}
                      </Text>
                    )}
                  </View>
                </View>
                <View style={{height: 95}}>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    ref={ref => {
                      inputs['5'] = ref;
                    }}
                    maxLength={100}
                    style={styles.input}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('6');
                    }}
                    autoCapitalize='none'
                    keyboardType='email-address'
                    returnKeyType={'next'}
                    value={values.email}
                    onChangeText={handleChange('email')}
                    onFocus={() => {
                      console.log('focus email')
                      setFieldTouched('email',false);
                    }}
                  />
                  {touched.email && errors.email && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.email}
                    </Text>
                  )}
                </View>
                <View style={{height: 95}}>
                  <Text style={styles.label}>Celular</Text>
                  <TextInputMask
                    type={'cel-phone'}
                    style={styles.input}
                    options={{
                      maskType: 'BRL',
                      withDDD: true,
                      dddMask: '(99) ',
                    }}
                    value={values.mobile}
                    blurOnSubmit={true}
                    returnKeyType={'next'}
                    ref={input => {
                      inputs['6'] = input;
                    }}
                    onBlur={() => {
                      const ddd = values.mobile.substr(0,4);
                      console.log(ddd);
                      
                      console.log(inputs['6'].isValid());
                    }}
                    onChangeText={handleChange('mobile')}
                    onFocus={() => setFieldTouched('mobile',false)}
                  />
                  {touched.mobile && errors.mobile && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.mobile}
                    </Text>
                  )}
                </View>
              </View>
              {/* {Object.values(errors).map(msg => {
                <>
                  <Text>{msg}</Text>
                </>
              })} */}
              <TouchableHighlight
                style={styles.button}
                onPress={() => {
                  Keyboard.dismiss();
                  handleSubmit();
                }}
                underlayColor="#fff">
                <Text style={styles.submitText}>Continuar</Text>
              </TouchableHighlight>
            </View>
          )}
        </Formik>
      </InputScrollView>
    </SafeAreaView>
  );
}
