import React, {useState} from 'react';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import {
  Alert, 
  AsyncStorage, 
  Platform,
  View
} from 'react-native';

import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

import { useFocusEffect } from '@react-navigation/native';
import {Header} from 'react-native-elements';

import styles from './styles.js';

const mapaStyle = [
  {
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'administrative',
    elementType: 'geometry.fill',
    stylers: [
      {
        visibility: 'simplified',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'administrative.neighborhood',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'landscape.man_made',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'landscape.natural.landcover',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f0b34b',
      },
    ],
  },
  {
    featureType: 'landscape.natural.terrain',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#e1a645',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#d6d6d6',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#c0c0c0',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#ebebeb',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#5c99ff',
      },
    ],
  },
];

function storeData(name, object){
  AsyncStorage.mergeItem(name, JSON.stringify(object), () => {
    AsyncStorage.getItem(name, (err, result) => {
      console.log(result);
      return result;
    });
  });
}

export default function Mapa(props) {
  const [marker, setMarker] = useState();
  const [region, setRegion] = useState({latitudeDelta: 0.0922,longitudeDelta: 0.0421});

  async function getLocationAsync(){
    const status = await Permissions.askAsync(Permissions.LOCATION);
    console.log(status);
    console.log(status.status !== 'granted');
    if (status.status !== 'granted') {
      Alert.alert('Permissão de acesso a Localização negada.');
      return null;
    } else {
      const start = Date.now();
      console.log(start);
      let geo = await Location.getCurrentPositionAsync({
        //enableHighAccuracy: true,
        //accuracy: Platform.OS === 'android' ? Location.Accuracy.Low : Location.Accuracy.Lowest,
        //distanceInterval: 2,
        maximumAge: 60000
      });
      console.log(geo);
      console.log(Date.now() - start);
      console.log(geo.coords);
      setMarker(<Marker
        coordinate={geo.coords}
        title="Você está aqui"
        description="Sua Localização"
      />);
      setRegion({
        latitude: geo.coords.latitude, 
        longitude: geo.coords.longitude, 
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421});
      let r = await AsyncStorage.setItem('local', JSON.stringify(region), () => {
        AsyncStorage.getItem('local', function(err, result){
          console.log(result);
        });
      });
      console.log(r);
      return geo;
    }
  };

  useFocusEffect(() => {
    AsyncStorage.getItem('user', function(err, result){
      if(result == null){
        props.navigation.navigate('Login');
      }
    });
    console.log(!region.hasOwnProperty('latitude'));
    if(!region.hasOwnProperty('latitude')){
      AsyncStorage.getItem('local', function(err, result){
        console.log(result);
      });
      getLocationAsync();
    }
  }, []);

  return (
      <View style={styles.container}>
        <Header
          backgroundColor='#fff'
          leftComponent={{ 
            icon: 'menu', 
            color: '#333',
            onPress: () => props.navigation.openDrawer()
          }}
          // rightComponent={{ icon: 'map', color: '#333' }}
        />
        <MapView 
          style={styles.mapStyle} 
          customMapStyle={mapaStyle} 
          provider={PROVIDER_GOOGLE}
          loadingEnabled={true}
          region={region}
          fitToElements={true}
          showsUserLocation={true}
          initialRegion={region}
          >
          {marker}
        </MapView>
      </View>
  );
}
