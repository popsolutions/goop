import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './pages/Login';
import Foto from './pages/Foto';
import CadastroForm1 from './pages/CadastroForm1';
import CadastroForm2 from './pages/CadastroForm2';
import Main from './pages/Main';
import Termos from './pages/Termos';

const Stack = createStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="CadastroForm1" component={CadastroForm1} />
        <Stack.Screen name="CadastroForm2" component={CadastroForm2} />
        <Stack.Screen name="Foto" component={Foto} />
        <Stack.Screen name="Termos" component={Termos} />
        <Stack.Screen name="Main" component={Main} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;