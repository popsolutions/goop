import React, {Fragment, useState, useEffect} from 'react';
import {LoginButton, AccessToken} from 'react-native-fbsdk';

import * as yup from 'yup';
import {Formik} from 'formik';

import {
  Alert,
  Image,
  StatusBar,
  TextInput,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

import styles from './styles.js';

export default function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const inputs = {};

  function focusNextField(id) {
    if (inputs[id].getElement) {
      inputs[id].getElement().focus();
    } else {
      inputs[id].focus();
    }
  }

  // useEffect(async () => {
  //   const response = await fetch('https://');
  //   const data = await response.json();
  //   console.log(data);

  //   return data;
  // }, []);

  return (
    <Formik
      initialValues={{email: email, password: password}}
      onSubmit={values => Alert.alert(JSON.stringify(values))}
      validationSchema={yup.object().shape({
        email: yup
          .string()
          .email('Digite um e-mail válido')
          .required('Preencha o Email'),
        password: yup
          .string()
          .min(6, 'A senha deve ter no mínimo 6 caracteres')
          .required('Preencha a Senha'),
      })}>
      {({
        values,
        handleChange,
        errors,
        setFieldTouched,
        touched,
        isValid,
        handleSubmit,
      }) => (
        <Fragment>
          <StatusBar translucent backgroundColor="#000000" />
          <View style={[styles.body]} height="100%">
            <View style={[styles.imageArea]}>
              <Image
                source={require('../../assets/img/logo.png')}
                style={{width: 200, height: 200}}
              />
            </View>
            <View style={styles.inputArea}>
              <View>
                <View>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    value={values.email}
                    onChangeText={handleChange('email')}
                    onBlur={() => setFieldTouched('email')}
                    style={styles.input}
                    keyboardType="email-address"
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('2');
                    }}
                    returnKeyType={'next'}
                    ref={input => {
                      inputs['1'] = input;
                    }}
                  />
                  {touched.email && errors.email && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.email}
                    </Text>
                  )}
                </View>
                <View style={{marginTop: 20}}>
                  <View>
                    <Text style={styles.label}>Senha</Text>
                    <TextInput
                      value={values.password}
                      onChangeText={handleChange('password')}
                      onBlur={() => setFieldTouched('password')}
                      secureTextEntry={true}
                      style={styles.input}
                      ref={input => {
                        inputs['2'] = input;
                      }}
                    />
                    {touched.password && errors.password && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.password}
                      </Text>
                    )}
                  </View>
                </View>
              </View>
              <TouchableHighlight
                style={styles.pinkButton}
                onPress={handleSubmit}
                disabled={!isValid}
                underlayColor="#fff">
                <Text style={styles.submitText}>Entrar</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={styles.facebook}
                onPress={() => props.navigation.navigate('Cadastro')}
                underlayColor="#fff">
                <Text style={styles.submitText}>ENTRAR COM FACEBOOK</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={styles.lightButton}
                onPress={() => props.navigation.navigate('Cadastro')}
                underlayColor="#fff">
                <Text style={styles.darkText}>Cadastre-se agora</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Fragment>
      )}
    </Formik>
  );
}
