import React, {useState} from 'react';
import * as yup from 'yup';
import {Formik} from 'formik';
import Swiper from 'react-native-swiper';
import {TextInputMask} from 'react-native-masked-text';
import ModalSelector from 'react-native-modal-selector';
import {CheckBox, Icon} from 'react-native-elements';

import {
  Alert,
  Image,
  Keyboard,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';

import styles from './styles.js';

const initialValues = {
  foto: '',
  nome: '',
  cpf: '',
  nascimento: '',
  genero: '',
  email: '',
  celular: '',
};

export default function Cadastro(props) {
  const foto = require('../../assets/img/placeholder.png');

  const swipeAllowed = true;

  const [modalVisible, setModalVisible] = useState(false);

  const [maior, setMaior] = useState(true);
  const [aceito, setAceito] = useState(false);

  const inputs = {};

  let index = 0;
  const data = [
    {key: index++, label: 'Feminino'},
    {key: index++, label: 'Masculino'},
  ];

  const initialValues2 = {
    cep: '',
    profissao: '',
    escolaridade: '',
    maior: maior,
    aceito: aceito,
  };

  function focusNextField(id) {
    if (inputs[id].getElement) {
      inputs[id].getElement().focus();
    } else {
      inputs[id].focus();
    }
  }

  const [genero, setGenero] = useState('');

  function onSubmit(values) {
    console.log(values);
    Alert.alert(JSON.stringify(values));
    Keyboard.dismiss();
  }

  return (
    <Swiper
      style={styles.wrapper}
      dot={<View style={styles.dot} />}
      activeDot={<View style={styles.activeDot} />}
      paginationStyle={styles.page}
      loop={false}
      scrollEnabled={swipeAllowed}>
      <View style={[styles.slide, styles.white]}>
        <>
          <View style={[styles.titleView, styles.white]}>
            <Text style={styles.title}>Cadastro</Text>
          </View>

          <Formik
            initialValues={initialValues}
            onSubmit={onSubmit.bind(this)}
            validationSchema={yup.object().shape({
              foto: yup.string().required('Envie a foto'),
              nome: yup.string().required('Preencha o Nome'),
              cpf: yup
                .string()
                .min(10, 'O CPF deve ter no mínimo 10 caracteres')
                .required('Preencha o CPF'),
              nascimento: yup
                .string()
                .min(10, 'DD/MM/AAAA')
                .required('Preencha a Data'),
              email: yup
                .string()
                .email('Digite um nome válido')
                .required('Preencha o Email'),
              celular: yup
                .string()
                .email('Digite um número válido')
                .required('Preencha o Celular'),
              genero: yup.string().required('Selecione o Gênero'),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
            }) => (
              <View style={[styles.body, styles.white]}>
                <View style={styles.alignItemsCenter}>
                  <TouchableOpacity
                    onPress={() => {
                      props.navigation.navigate('Camera');
                    }}>
                    <View>
                      <Image source={foto} style={styles.image} />
                    </View>
                    <Text style={styles.imageLabel}>Foto</Text>
                  </TouchableOpacity>
                  {touched.foto && errors.foto && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.foto}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.label}>Nome Completo</Text>
                  <TextInput
                    value={values.nome}
                    onChangeText={handleChange('nome')}
                    onBlur={() => setFieldTouched('nome')}
                    style={styles.input}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('2');
                    }}
                    returnKeyType={'next'}
                    ref={input => {
                      inputs['1'] = input;
                    }}
                  />
                  {touched.nome && errors.nome && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.nome}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.label}>CPF</Text>
                  <TextInputMask
                    type={'cpf'}
                    style={styles.input}
                    value={values.cpf}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('3');
                    }}
                    returnKeyType={'next'}
                    onChangeText={handleChange('cpf')}
                    onBlur={() => setFieldTouched('cpf')}
                    ref={ref => (inputs['2'] = ref)}
                  />
                  {touched.cpf && errors.cpf && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.cpf}
                    </Text>
                  )}
                </View>
                <View style={styles.twoRows}>
                  <View style={styles.column}>
                    <Text style={styles.label}>Nascimento</Text>
                    <TextInputMask
                      type={'datetime'}
                      style={styles.input}
                      value={values.nascimento}
                      blurOnSubmit={false}
                      onSubmitEditing={() => {
                        if (errors.nascimento) {
                          setModalVisible(true);
                        }
                      }}
                      options={{
                        format: 'DD/MM/YYYY',
                      }}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['3'] = input;
                      }}
                      onChangeText={handleChange('nascimento')}
                      onBlur={() => setFieldTouched('nascimento')}
                    />
                    {touched.nascimento && errors.nascimento && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.nascimento}
                      </Text>
                    )}
                  </View>
                  <View style={styles.column}>
                    <Text style={styles.label}>Gênero</Text>
                    <ModalSelector
                      data={data}
                      initValue=" "
                      style={styles.modalSelector}
                      optionContainerStyle={styles.white}
                      cancelContainerStyle={styles.cancelContainerStyle}
                      touchableActiveOpacity={1}
                      optionStyle={styles.optionStyle}
                      optionTextStyle={styles.white}
                      selectTextStyle={styles.selectTextStyle}
                      touchableStyle={styles.touchableStyle}
                      selectStyle={styles.selectStyle}
                      childrenContainerStyle={styles.childrenContainerStyle}
                      initValueTextStyle={styles.initValueTextStyle}
                      visible={modalVisible}
                      onModalClose={item => {
                        setGenero(item.label);
                        setFieldTouched('genero');
                        if (item.key === 0 || item.key === 1) {
                          handleChange('genero');
                          setTimeout(() => {
                            focusNextField('5');
                          }, 50);
                        }
                      }}
                    />
                    {touched.genero && errors.genero && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.genero}
                      </Text>
                    )}
                  </View>
                </View>
                <View>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    style={styles.input}
                    keyboardType="email-address"
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('6');
                    }}
                    returnKeyType={'next'}
                    ref={input => {
                      inputs['5'] = input;
                    }}
                    value={values.email}
                    onChangeText={handleChange('email')}
                    onBlur={() => setFieldTouched('email')}
                  />
                  {touched.email && errors.email && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.email}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.label}>Celular</Text>
                  <TextInputMask
                    type={'cel-phone'}
                    style={styles.input}
                    options={{
                      maskType: 'BRL',
                      withDDD: true,
                      dddMask: '(99) ',
                    }}
                    value={values.celular}
                    blurOnSubmit={true}
                    returnKeyType={'next'}
                    ref={input => {
                      inputs['6'] = input;
                    }}
                    onChangeText={handleChange('celular')}
                    onBlur={() => setFieldTouched('celular')}
                  />
                  {touched.celular && errors.celular && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.celular}
                    </Text>
                  )}
                </View>
                <TouchableHighlight
                  style={styles.button}
                  onPress={handleSubmit}
                  underlayColor="#fff">
                  <Text style={styles.submitText}>Continuar</Text>
                </TouchableHighlight>
              </View>
            )}
          </Formik>
        </>
      </View>

      <View style={[styles.slide, styles.white]}>
        <>
          <View style={[styles.titleView, styles.white]}>
            <Text style={styles.title}>Cadastro</Text>
          </View>

          <Formik
            initialValues={initialValues2}
            onSubmit={onSubmit.bind(this)}
            validationSchema={yup.object().shape({
              cep: yup
                .string()
                .min(8, 'O CEP deve ter 8 números')
                .required('Preencha o CEP'),
              escolaridade: yup.string().required('Preencha a Escolaridade'),
              profissao: yup.string().required('Preencha a Profissão'),
              maior: yup
                .boolean()
                .required('Você deve ser maior de 18 anos')
                .oneOf([true], 'Você deve ser maior de 18 anos'),
              aceito: yup
                .boolean()
                .required('Você deve aceitar os Termos e Condições')
                .oneOf([true], 'Você deve aceitar os Termos e Condições'),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
            }) => (
              <View style={[styles.body, styles.white]}>
                <View>
                  <Text style={styles.label}>CEP Residencial</Text>
                  <TextInputMask
                    type={'custom'}
                    options={{
                      mask: '99999-999',
                    }}
                    style={styles.input}
                    value={values.cep}
                    blurOnSubmit={false}
                    onSubmitEditing={() => {
                      focusNextField('8');
                    }}
                    returnKeyType={'next'}
                    onChangeText={handleChange('cep')}
                    onBlur={() => setFieldTouched('cep')}
                  />
                  {touched.cep && errors.cep && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.cep}
                    </Text>
                  )}
                </View>
                <View style={styles.twoRows}>
                  <View style={styles.column}>
                    <Text style={styles.label}>Escolaridade</Text>
                    <TextInput
                      style={styles.input}
                      value={values.escolaridade}
                      blurOnSubmit={false}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['8'] = input;
                      }}
                      onSubmitEditing={() => {
                        focusNextField('9');
                      }}
                      onChangeText={handleChange('escolaridade')}
                      onBlur={() => setFieldTouched('escolaridade')}
                    />
                    {touched.escolaridade && errors.escolaridade && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.escolaridade}
                      </Text>
                    )}
                  </View>
                  <View style={styles.column}>
                    <Text style={styles.label}>Profissão</Text>
                    <TextInput
                      style={styles.input}
                      value={values.profissao}
                      blurOnSubmit={false}
                      returnKeyType={'next'}
                      ref={input => {
                        inputs['10'] = input;
                      }}
                      onSubmitEditing={() => {
                        focusNextField('11');
                      }}
                      onChangeText={handleChange('profissao')}
                      onBlur={() => setFieldTouched('profissao')}
                    />
                    {touched.profissao && errors.profissao && (
                      <Text style={[styles.label, styles.error]}>
                        {errors.profissao}
                      </Text>
                    )}
                  </View>
                </View>
                <View>
                  <CheckBox
                    title="Sou maior de 18 anos."
                    containerStyle={[
                      styles.label,
                      styles.childrenContainerStyle,
                    ]}
                    checked={values.maior}
                    onPress={() => setMaior(!values.maior)}
                  />
                  {touched.maior && errors.maior && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.maior}
                    </Text>
                  )}
                </View>
                <View>
                  <CheckBox
                    title="Aceito os Termos de Uso."
                    iconType="material"
                    containerStyle={[
                      styles.label,
                      styles.childrenContainerStyle,
                    ]}
                    checked={values.aceito}
                    onPress={() => {
                      values.aceito = !values.aceito;
                      setAceito(!values.aceito);
                    }}
                  />
                  {touched.aceito && errors.aceito && (
                    <Text style={[styles.label, styles.error]}>
                      {errors.aceito}
                    </Text>
                  )}
                </View>
                <Icon
                  raised
                  name="heartbeat"
                  type="font-awesome"
                  color="#f50"
                  onPress={() => console.log('hello')}
                />
                <TouchableHighlight
                  style={styles.button}
                  onPress={() => props.navigation.navigate('Mapa')}
                  underlayColor="#fff">
                  <Text style={styles.submitText}>Finalizar</Text>
                </TouchableHighlight>
              </View>
            )}
          </Formik>
        </>
      </View>
    </Swiper>
  );
}
