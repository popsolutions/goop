import {StyleSheet} from 'react-native';
import Constants from 'expo-constants';

const styles = StyleSheet.create({
  wrapper: {},
  white: {
    backgroundColor: '#fff',
  },
  slide: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    marginTop: 10,
    paddingBottom: 20,
  },
  dot: {
    backgroundColor: 'rgba(228, 0, 99,.3)',
    width: 13,
    height: 13,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    paddingTop: 10,
  },
  activeDot: {
    backgroundColor: '#e40063',
    width: 13,
    height: 13,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
  },
  body: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'space-between',
    paddingBottom: 30,
  },
  page: {bottom: 10},
  image: {
    width: 130,
    height: 130,
  },
  imageLabel: {
    fontSize: 14,
    color: '#fff',
    alignSelf: 'center',
    position: 'absolute',
    marginTop: 100,
  },
  titleView: {
    alignSelf: 'stretch',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  title: {
    fontSize: 24,
    color: '#000',
  },
  inputArea: {
    flex: 1,
    justifyContent: 'space-between',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 10,
    textAlign: 'center',
    color: '#858585',
  },
  initValueTextStyle: {
    display: 'none',
  },
  cancelContainerStyle: {
    height: 0,
    overflow: 'hidden',
  },
  optionStyle: {
    backgroundColor: 'white',
    padding: 15,
  },
  twoRows: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  column: {flexGrow: 1, flexBasis: 0},
  modalSelector: {
    backgroundColor: '#fff',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 0,
    padding: 0,
  },
  selectTextStyle: {
    color: '#858585',
    backgroundColor: '#fff',
    borderWidth: 0,
    padding: 0,
    marginTop: -3,
  },
  touchableStyle: {
    height: 38,
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 0,
  },
  selectStyle: {
    borderWidth: 0,
    height: 38,
    marginBottom: 0,
    borderRadius: 10,
  },
  childrenContainerStyle: {
    height: 38,
    backgroundColor: '#fff',
    borderWidth: 0,
    borderRadius: 10,
    padding: 0,
  },
  submitText: {
    color: '#fff',
    fontSize: 14,
    alignSelf: 'center',
  },
  darkText: {
    color: '#000',
    alignSelf: 'center',
  },
  alignItemsCenter: {alignItems: 'center'},
  lightButton: {
    alignSelf: 'center',
    padding: 5,
    marginBottom: 40,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  droidSafeArea: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
  },
  label: {
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 10,
    marginBottom: 5,
  },
  error: {
    fontSize: 10,
    color: 'red',
  },
  button: {
    backgroundColor: '#2d2e49',
    borderColor: '#2d2e49',
    marginRight: 130,
    marginLeft: 130,
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: 10,
    borderWidth: 1,
  },
});

export default styles;
