import React, {useEffect, useState} from 'react';
import MapView, {Marker} from 'react-native-maps';
import {Alert, View} from 'react-native';

import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

import styles from './styles.js';

const mapa = [
  {
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'administrative',
    elementType: 'geometry.fill',
    stylers: [
      {
        visibility: 'simplified',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'administrative.neighborhood',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'landscape.man_made',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f9b84c',
      },
    ],
  },
  {
    featureType: 'landscape.natural.landcover',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#f0b34b',
      },
    ],
  },
  {
    featureType: 'landscape.natural.terrain',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#e1a645',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#d6d6d6',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#c0c0c0',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#ebebeb',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#5c99ff',
      },
    ],
  },
];

export default function Mapa(props) {
  const [marker, setMarker] = useState();

  async function _getLocationAsync() {
    let {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      Alert.alert('Permission to access location was denied');
      return null;
    } else {
      const geo = await Location.getCurrentPositionAsync({
        enableHighAccuracy: false,
      });
      return geo;
      //Alert.alert(JSON.stringify(geo.coords));
      // if (geo.coords) {
      //   setMarker();
      //   // <Marker
      //   //   coordinate={geo.coords}
      //   //   title="Você está aqui"
      //   //   description="Sua Localização"
      //   // />,
      // }
    }
  }

  useEffect(() => {
    _getLocationAsync();
  }, []);

  return (
    <View style={styles.container}>
      <MapView style={styles.mapStyle} customMapStyle={mapa} />
    </View>
  );
}
