import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Login from './pages/Login';
import Cadastro from './pages/Cadastro';
import Mapa from './pages/Mapa';

const mainNavigation = createStackNavigator(
  {
    Login,
    Cadastro,
    Mapa,
  },
  {
    headerMode: 'none',
    header: null,
  },
);

const Routes = createAppContainer(mainNavigation);

export default Routes;
