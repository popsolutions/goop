import styled from 'styled-components/native';

export default (Button = styled.Button`
  marginRight: 130,
  marginLeft: 130,
  paddingTop: 15,
  paddingBottom: 15,
  backgroundColor: '#e40063',
  borderRadius: 10,
  borderWidth: 1,
  borderColor: '#e40063',
`);
