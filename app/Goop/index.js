import 'react-native-gesture-handler';

import {AppRegistry, StatusBar} from 'react-native';
import React from 'react';

import Routes from './src/routes';

export default function App() {
  return (
    <>
      <StatusBar
        backgroundColor="#000000"
        translucent="false"
        barStyle="light-content"
      />
      <Routes />
    </>
  );
}

AppRegistry.registerComponent('main', () => App);
